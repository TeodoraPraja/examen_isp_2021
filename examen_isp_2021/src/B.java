import java.util.List;

class A{
}

public class B extends A {
    private String param;
    private List<D> list_of_D;

    public B(String param)
    {
        this.param=param;
    }

    public void x(){

    }
    public void y(){

    }
}

class C{
   B attribute_B;

   public C(){
       attribute_B = new B(); // composition
   }
}

class D{

}

class E{

}

class U{

}

